package com.itcse.rohankandwal.navigationdrawer;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;

    private boolean mUserLearnedDrawer;
    boolean mFromSavedInstance;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Checking if the user has already seen the NavigationDrawerMenu first time
        mUserLearnedDrawer = Boolean.valueOf(readFromPreference(getActivity(), "userLearned",
                "false"));
        // Checking if the device's screen has rotated
        if (savedInstanceState!=null)
            mFromSavedInstance = true;
    }

    public void setUp(DrawerLayout drawerLayout, final Toolbar toolbar){
        this.toolbar = toolbar;
        this.drawerLayout = drawerLayout;
        // Setting up Drawer Toggle
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar,
                R.string.DrawerOpened, (R.string.DrawerClosed)){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Checking if the user has seen the Drawer for the first time, if so,
                // save in SharedPreferences so that Drawer isn't shown automatically from next time
                if (!mUserLearnedDrawer){
                    mUserLearnedDrawer = true;
                    saveToPreference(getActivity(), "userLearned", "true");
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();

            }


        };

        // Setting DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);

        //
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        if (!mUserLearnedDrawer && !mFromSavedInstance)
            drawerLayout.openDrawer(Gravity.START);
    }

    public void saveToPreference(Context mContext, String preferenceName,
                                 String preferenceValue) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("SharedPreference.xml", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public String readFromPreference(Context mContext, String preferenceName,
                                     String defaultValue) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("SharedPreference.xml", Context.MODE_PRIVATE);
       return sharedPreferences.getString(preferenceName, defaultValue);
    }
}
