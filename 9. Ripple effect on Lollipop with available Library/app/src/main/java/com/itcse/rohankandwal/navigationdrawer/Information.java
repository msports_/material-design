package com.itcse.rohankandwal.navigationdrawer;

/**
 * Created by User on 27-04-2015.
 */
public class Information {
    int resIdImage;
    String text;

    public int getResIdImage() {
        return resIdImage;
    }

    public void setResIdImage(int resIdImage) {
        this.resIdImage = resIdImage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
