package com.itcse.rohankandwal.navigationdrawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

/**
 * Created by User on 27-04-2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    LayoutInflater inflater;
    List<Information> data = Collections.emptyList();
    Context context;
    ClickListener clickListener;
    public RecyclerAdapter(Context context, List<Information> data){
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.context = context;
    }
    public void setClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = inflater.inflate(R.layout.custom_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int i) {
        Information information = data.get(i);
        viewHolder.text.setText(information.getText());
        viewHolder.imageView.setImageResource(information.getResIdImage());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView text;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);

            text = (TextView) itemView.findViewById(R.id.text);
            imageView = (ImageView) itemView.findViewById(R.id.image);
//          First technique
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "View clicked at position=" + getPosition(), Toast.LENGTH_SHORT).show();


/*            if (clickListener!=null)
                clickListener.itemClicked(v, getPosition());*/
        }
    }

//    Second Technique
    public interface ClickListener{
        public void itemClicked(View view, int position);
    }
}
