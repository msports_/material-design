package com.itcse.rohankandwal.navigationdrawer;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment implements RecyclerAdapter.ClickListener{
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    private boolean mUserLearnedDrawer;
    boolean mFromSavedInstance;

    RecyclerAdapter.ClickListener clickListener;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);;
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerAdapter  = new RecyclerAdapter(getActivity(), getData());
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                startActivity(new Intent(getActivity(), SubActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
                startActivity(new Intent(getActivity(), SubActivity.class));

            }
        } ));*/
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Checking if the user has already seen the NavigationDrawerMenu first time
        mUserLearnedDrawer = Boolean.valueOf(readFromPreference(getActivity(), "userLearned",
                "false"));
        // Checking if the device's screen has rotated
        if (savedInstanceState!=null)
            mFromSavedInstance = true;
    }

    public void setUp(DrawerLayout drawerLayout, Toolbar toolbar){
        this.toolbar = toolbar;
        this.drawerLayout = drawerLayout;
        // Setting up Drawer Toggle
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar,
                R.string.DrawerOpened, (R.string.DrawerClosed)){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // Checking if the user has seen the Drawer for the first time, if so,
                // save in SharedPreferences so that Drawer isn't shown automatically from next time
                if (!mUserLearnedDrawer){
                    mUserLearnedDrawer = true;
                    saveToPreference(getActivity(), "userLearned", "true");
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();

            }

        };

        // Setting DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);

        //
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        if (!mUserLearnedDrawer && !mFromSavedInstance)
            drawerLayout.openDrawer(Gravity.START);
    }

    public List<Information> getData(){
        List<Information> data = new ArrayList<>();
        int[] icons = {R.drawable.abc_ic_ab_back_mtrl_am_alpha, R.drawable.abc_ic_ab_back_mtrl_am_alpha,
                R.drawable.abc_ic_ab_back_mtrl_am_alpha};
        String[] title = {"Title 1", "Title 2", "Title 3"};

        for (int i=0; i<icons.length; i++) {
            Information information = new Information();
            information.setResIdImage(icons[i]);
            information.setText(title[i]);
            data.add(information);
        }
        return data;
    }

    public void saveToPreference(Context mContext, String preferenceName,
                                 String preferenceValue) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("SharedPreference.xml", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public String readFromPreference(Context mContext, String preferenceName,
                                     String defaultValue) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("SharedPreference.xml", Context.MODE_PRIVATE);
       return sharedPreferences.getString(preferenceName, defaultValue);
    }

    @Override
    public void itemClicked(View view, int position) {
        startActivity(new Intent(getActivity(), SubActivity.class));

    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        GestureDetector gestureDetector;
        ClickListener clickListener;

        public RecyclerTouchListener(Context context,
                                     final RecyclerView recyclerView, ClickListener clickListener){
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context,
                    new GestureDetector.SimpleOnGestureListener(){
                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                            if (child!=null && RecyclerTouchListener.this.clickListener!=null){
                                RecyclerTouchListener.this.clickListener.onClick(child, recyclerView.getChildPosition(child));
                            }
                            return true;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                            super.onLongPress(e);
                            View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                            if (child!=null && RecyclerTouchListener.this.clickListener!=null){
                                RecyclerTouchListener.this.clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                            }
                        }
                    });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }
    }

    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }
}
